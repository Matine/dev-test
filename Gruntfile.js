module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: {
                    'css/style.css' : 'css/scss/style.scss'
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'css/lib.css': ['css/lib/normalize.css', 'css/lib/main.css', 'css/lib/bootstrap.css'],
                    'css/style.css': ['css/style.css']
                }
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default',['sass', 'cssmin']);
}
