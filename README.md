# [Dev Test](https://bitbucket.org/Matine/dev-test)

A Dev test for Syzygy

## Technologies/tools used

* HTML5 Boilerplate
* SCSS (Also using some principles from SMACSS and a modular file structure)
* Bootstrap (For the responsive grid only)
* Grunt (Compile SCSS, Minify CSS, Watch)